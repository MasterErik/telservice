package com.gmail.ivanov.erik.telservice.model;

import java.util.List;
import java.util.Map;

/**
 * User: Erik
 * Date: 04.10.13
 * Time: 13:34
 */
public class CallRec {
    private int sequenceId;
    private String service;
    private String serviceXL;
    private String phoneNumber;
    private String language;
    private String languageXL;
    private String serviceDate;
    private String serviceTime;
    private String serviceTimeXLstart;
    private String serviceTimeXLend;
    private List<Map<String,String>> callList;

    public int getSequenceId() {
        return sequenceId;
    }

    public String getService() {
        return service;
    }

    public String getServiceXL() {
        return serviceXL;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getLanguage() {
        return language;
    }

    public String getLanguageXL() {
        return languageXL;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public String getServiceTimeXLstart() {
        return serviceTimeXLstart;
    }

    public String getServiceTimeXLend() {
        return serviceTimeXLend;
    }

    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setServiceXL(String serviceXL) {
        this.serviceXL = serviceXL;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setLanguageXL(String languageXL) {
        this.languageXL = languageXL;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public void setServiceTimeXLstart(String serviceTimeXLstart) {
        this.serviceTimeXLstart = serviceTimeXLstart;
    }

    public void setServiceTimeXLend(String serviceTimeXLend) {
        this.serviceTimeXLend = serviceTimeXLend;
    }

    public List<Map<String, String>> getCallList() {
        return callList;
    }

    public void setCallList(List<Map<String, String>> callList) {
        this.callList = callList;
    }

}

