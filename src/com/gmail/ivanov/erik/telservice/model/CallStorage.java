package com.gmail.ivanov.erik.telservice.model;

public interface CallStorage {
    public void setBuffer(String value, int SequenceId);
    public int getSequenceId();
    public String getBuffer();
}
