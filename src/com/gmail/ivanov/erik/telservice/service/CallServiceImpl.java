package com.gmail.ivanov.erik.telservice.service;

import com.gmail.ivanov.erik.telservice.model.CallRec;
import com.gmail.ivanov.erik.telservice.model.CallStorage;
import com.gmail.ivanov.erik.telservice.model.parser.ParserCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;



@Service
public class CallServiceImpl implements CallService {
 
    @Autowired
    private CallStorage callStorage;

 
    public Map<String, Object> mapInfo(){
        CallRec callRec = ParserCall.parse(callStorage);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sequenceId", callRec.getSequenceId());
        map.put("phoneNumber", callRec.getPhoneNumber() );
        map.put("language", callRec.getLanguage());
        map.put("service", callRec.getService());
        map.put("serviceXL", callRec.getServiceXL());
        if (callRec.getService() == "active") {
            map.put("serviceDate", callRec.getServiceDate());
            map.put("serviceTime", callRec.getServiceTime());
            if (callRec.getServiceXL() == "active") {
                map.put("languageXL", callRec.getLanguageXL());
                map.put("serviceTimeXLstart", callRec.getServiceTimeXLstart());
                map.put("serviceTimeXLend", callRec.getServiceTimeXLend());
            }
        }
        map.put("callList", callRec.getCallList());

        return map;
    }
 
}
