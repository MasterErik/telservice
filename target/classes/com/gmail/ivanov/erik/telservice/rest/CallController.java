package com.gmail.ivanov.erik.telservice.rest;

import com.gmail.ivanov.erik.telservice.service.CallService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("index")
@Produces("application/json")
public class CallController extends SpringAwareResource {
    @Autowired
    private CallService callService;

    @GET
    public Map<String, Object> getAllData() {
        Map<String, Object> map = callService.mapInfo();

      return map;
    }

}
