package com.gmail.ivanov.erik.telservice.model.parser;

import com.gmail.ivanov.erik.telservice.model.CallRec;
import com.gmail.ivanov.erik.telservice.model.CallStorage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: Erik
 * Date: 04.10.13
 * Time: 13:58
 */
public class ParserCall {
    public static CallRec parse(CallStorage callStorage){
        int id = callStorage.getSequenceId();
        String value = callStorage.getBuffer();

        CallRec callRec = new CallRec();
        callRec.setSequenceId(id);
        if (value != null && value != ""){
            callRec.setService(getService(value));
            callRec.setServiceXL(getServiceXL(value));
            callRec.setPhoneNumber(getPhoneNumber(value));
            callRec.setLanguage(getLanguage(value));
            callRec.setLanguageXL(getLanguageXL(value));
            callRec.setServiceDate(getServiceDate(value));
            callRec.setServiceTime(getServiceTime(value));
            callRec.setServiceTimeXLstart(getServiceTimeXLstart(value));
            callRec.setServiceTimeXLend(getServiceTimeXLend(value));
            callRec.setCallList(callList(value));
        }
        return callRec;
    }

    private static String getLang(char value) {
        switch (value) {
            case 'E':
                return "Estonian";
            case 'I':
                return "English";
            default:
                return "";
        }
    }

    private static final String getService(String value) {
        if (!value.isEmpty()) {
            switch (value.charAt(0)) {
                case 'A':
                    return "active";
                case 'P':
                    return "inactive";
                default:
                    return "";
            }
        } else {
            return "";
        }
    }

    private static final String getServiceXL(String value) {
        switch (value.charAt(21)) {
            case 'J':
                return "active";
            case 'E':
                return "inactive";
            default:
                return "";
        }
    }

    private static final String getPhoneNumber(String value) {
        return value.substring(1, 20).trim();

    }

    private static final String getLanguage(String value) {
        return getLang(value.charAt(22));
    }

    private static final String getLanguageXL(String value) {
        return getLang(value.charAt(23));
    }

    private static String splitDate(String value) {
        return value.substring(0, 4) + "-" + value.substring(4, 6) + "-" + value.substring(6, 8);
    }
    private static final String getServiceDate(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date;
        try {
            date = formatter.parse(splitDate(value.substring(24, 32)));
        } catch (ParseException exception) {
            return "";
        }
        formatter.applyPattern("MMMM dd yyyy");
        return formatter.format(date);
    }

    private static String splitTime(String value) {
        return value.substring(0, 2) + ":" + value.substring(2, 4);
    }
    private static final String getServiceTime(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.US);
        Date time;
        try {
            time = formatter.parse(splitTime(value.substring(32, 36)));
        } catch (ParseException exception) {
            return "";
        }
        return formatter.format(time);
    }

    private static final String getServiceTimeXLstart(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.US);
        Date time;
        try {
            time = formatter.parse(splitTime(value.substring(36, 40)));
        } catch (ParseException exception) {
            return "";
        }
        return formatter.format(time);
    }

    private static final String getServiceTimeXLend(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.US);
        Date time;
        try {
            time = formatter.parse(splitTime(value.substring(40, 44)));
        } catch (ParseException exception) {
            return "";
        }
        return formatter.format(time);
    }

    private static boolean isList(String value) {
        switch (value.charAt(44)) {
            case 'K':
                return true;
            case 'E':
                return false;
            default:
                return false;
        }
    }

    private static final List<Map<String,String>> callList(String value) {
        List telList = new ArrayList<Map<String, Map>>();
        if (isList(value)) {
            int i = 45;
            int j = 45+120;
            int len = value.length();
            while (j < len) {
                Map record = new HashMap<String, String>();
                record.put("phone", value.substring(i, i + 15).trim());
                record.put("name", value.substring(j, Math.min(j + 20, len)).trim());
                telList.add(record);
                i = i + 15;
                j = j + 20;
            }
        }
        return telList;
    }

}
