/**
 * User: Erik
 * Date: 11.09.13
 * Time: 17:31
 */

package com.gmail.ivanov.erik.telservice.model;


import org.springframework.stereotype.Component;

@Component
public class CallStorageImpl implements CallStorage {
    private static volatile String buffer = "";
    private static volatile int sequenceId;

    public void setBuffer(String value, int sequenceId) {
        this.sequenceId = sequenceId;
        this.buffer = value;
    }

    public int getSequenceId() {
        return sequenceId;
    };
    public String getBuffer() {
        return buffer;
    };

}
