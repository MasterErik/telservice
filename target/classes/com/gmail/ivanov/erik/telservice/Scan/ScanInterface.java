package com.gmail.ivanov.erik.telservice.Scan;

import com.gmail.ivanov.erik.telservice.model.CallStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * User: Erik
 * Date: 15.09.13
 * Time: 1:56
 */
@Component
public class ScanInterface {
    @Autowired
    private CallStorage callStorage;

    private String question = "http://people.proekspert.ee/ak/data_?.txt";
    int count = 0;

    @Scheduled(fixedRate = 5000)
    public void run() {
        count++;
        if (count > 9) { count = 1; }
        try {
            String curQuestion = question.replace("?", String.valueOf(count));
            URL url = new URL(curQuestion);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine, body = "";
            while ((inputLine = reader.readLine()) != null) {
                body += inputLine;
            }
            reader.close();
            callStorage.setBuffer(body.trim(), count);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            callStorage.setBuffer("", count);
        }

    }

}
