import com.gmail.ivanov.erik.telservice.model.CallRec;
import com.gmail.ivanov.erik.telservice.model.CallStorage;
import com.gmail.ivanov.erik.telservice.model.CallStorageImpl;
import com.gmail.ivanov.erik.telservice.model.parser.ParserCall;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.springframework.test.util.AssertionErrors.assertTrue;


//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({ "spring.xml" }) //classpath:

public class ParserTest {
//    @Autowired
    private CallRec callRec;

    @Before
    public void initCall() throws Exception {
        CallStorage callStorage = new CallStorageImpl();
        callStorage.setBuffer("A0551234555          JII20111111215900001200K0551111111     0509999999                                                                                               Jaan Juurikas       Peeter", 1);
        callRec = ParserCall.parse(callStorage);
    }


    @Test
    public void sequenceTest() {
        assertTrue("sequence error", callRec.getSequenceId() == 1);
    }

    @Test
    public void serviceTest() {
        assertTrue("Service error", callRec.getService().equals("active"));
    }
    @Test
    public void phoneNumberTest() {
        assertTrue("phoneNumber error", callRec.getPhoneNumber().equals("0551234555"));
    }

    @Test
    public void serviceXLTest() {
        assertTrue("Service XL error", callRec.getServiceXL().equals("active"));
    }

    @Test
    public void languageTest() {
        assertTrue("Language error", callRec.getLanguage().equals("English"));
    }
    @Test
    public void languageXLTest() {
        assertTrue("Language error", callRec.getLanguageXL().equals("English"));
    }


    @Test
    public void serviceDateTest() {
        assertTrue("ServiceDate error", callRec.getServiceDate().equals("November 11 2011"));
    }
    @Test
    public void serviceTimeTest() {
        assertTrue("ServiceTime error", callRec.getServiceTime().equals("21:59"));
    }

    @Test
    public void serviceTimeXLstartTest() {
        assertTrue("XL Service activation time error", callRec.getServiceTimeXLstart().equals("00:00"));
    }
    @Test
    public void serviceTimeXLendTest() {
        assertTrue("XL getService end time error", callRec.getServiceTimeXLend().equals("12:00"));
    }
    @Test
    public void callListTest() {
        List telList = callRec.getCallList();
        Map record = (Map)telList.get(1);
        assertTrue("List error", (telList.size() == 2) && record.get("phone").equals("0509999999") && record.get("name").equals("Peeter") );
    }

}
